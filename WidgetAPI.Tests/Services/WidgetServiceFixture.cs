using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Logging;
using Moq;
using WidgetAPI.Factories;
using WidgetAPI.Services;
using System;
using AutoFixture;
using System.Collections.Generic;
using WidgetAPI.Models;
using System.Linq;
using Microsoft.Extensions.Logging.Internal;

namespace WidgetAPI.Tests
{
    // constructor initialises dependencies
    // widgets are packed
    // invalid colours throw an exception - data parameter testing
    // exceptions are handled

    [TestClass]
    public class WidgetServiceFixture
    {
        private readonly Fixture _fixture = new Fixture();

        private Mock<ILogger<WidgetService>> _loggerMock;

        private Mock<IFactory> _factoryMock;

        private Widget widget;

        private List<Widget> expectedWidgets;

        [TestInitialize]
        public void Setup()
        {
            widget = _fixture.Create<Widget>();
            expectedWidgets = new List<Widget>()
            {
                widget,
                widget,
                widget,
                widget,
                widget,
                widget
            };

            _loggerMock = new Mock<ILogger<WidgetService>>();
            _factoryMock = new Mock<IFactory>();
            _factoryMock.Setup(a => a.Make(It.IsAny<int>(), It.IsAny<string>())).Returns(widget);
        }

        [TestMethod]
        public void Constructor_Parameters_AreValidated()
        {
            // act
            var ex = Assert.ThrowsException<NullReferenceException>(() => new WidgetService(_loggerMock.Object, null));
            var ex1 = Assert.ThrowsException<NullReferenceException>(() => new WidgetService(null, _factoryMock.Object));

            // assert
            Assert.AreEqual(ex.Message, "Parameters must be provided to the WidgetService");
            Assert.AreEqual(ex1.Message, "Parameters must be provided to the WidgetService");
        }

        [TestMethod]
        public void PackWidgets_ValidInput_CreatesList()
        {
            // arrange 
            WidgetService widgetService = new WidgetService(_loggerMock.Object, _factoryMock.Object);

            // act
            var widgets = widgetService.PackWidgets(widget.Id, widget.Colour, expectedWidgets.Count);

            // assert
            CollectionAssert.AreEqual(expectedWidgets, widgets.ToList());
            _factoryMock.Verify(f => f.Make(It.IsAny<int>(), It.IsAny<string>()), Times.Exactly(expectedWidgets.Count()));
        }

        [TestMethod]
        public void PackWidgets_Error_RethrowsExceptions()
        {
            // arrange 
            var exceptionMessage = _fixture.Create<string>();
            _factoryMock.Setup(a => a.Make(It.IsAny<int>(), It.IsAny<string>())).Throws(new FormatException(exceptionMessage));
            WidgetService widgetService = new WidgetService(_loggerMock.Object, _factoryMock.Object);

            // act and assert
            var ex = Assert.ThrowsException<FormatException>(() => widgetService.PackWidgets(widget.Id, widget.Colour, expectedWidgets.Count));
            Assert.AreEqual(ex.Message, exceptionMessage);
            _loggerMock.Verify(x => x.Log(LogLevel.Error, It.IsAny<EventId>(), It.IsAny<FormattedLogValues>(), It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()), Times.Once);
            _factoryMock.Verify(f => f.Make(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        [DataTestMethod]
        [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
        public void PackWidgets_InvalidColours_ThrowsFormatException(string invalidColour)
        {
            // arrange 
            var validId = _fixture.Create<int>();

            WidgetService widgetService = new WidgetService(_loggerMock.Object, _factoryMock.Object);

            // act
            var ex = Assert.ThrowsException<FormatException>(()=> widgetService.PackWidgets(validId, invalidColour, expectedWidgets.Count));

            // assert
            Assert.AreEqual(ex.Message, "Widgets need a colour");
            _factoryMock.Verify(f => f.Make(It.IsAny<int>(), It.IsAny<string>()), Times.Never);
        }

        private static IEnumerable<object[]> GetData()
        {
            yield return new object[] { null };
            yield return new object[] { "" };
            yield return new object[] { " " };
        }
    }
}
