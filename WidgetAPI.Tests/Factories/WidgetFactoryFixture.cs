using System;
using AutoFixture;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WidgetAPI.Factories;

namespace WidgetAPI.Tests
{
    // negative ids throw exception
    // widgets are created

    [TestClass]
    public class WidgetFactoryFixture
    {
        private readonly Fixture _fixture = new Fixture();

        [TestMethod]
        public void Make_NegativeIds_ThrowsException()
        {
            // arrange
            var colour = _fixture.Create<string>();
            var id = -1;
            WidgetFactory widgetFactory = new WidgetFactory();

            // act
            var ex = Assert.ThrowsException<ApplicationException>(() => widgetFactory.Make(id, colour));

            //assert
            Assert.AreEqual(ex.Message, "Widgets must have an id greater than 0");
        }

        [TestMethod]
        public void Make_Create_ReturnsAWidget()
        {
            // arrange
            var colour = _fixture.Create<string>();
            var id = _fixture.Create<int>();
            WidgetFactory widgetFactory = new WidgetFactory();

            // act
            var widget = widgetFactory.Make(id, colour);

            // assert
            Assert.AreEqual(widget.Colour, colour);
            Assert.AreEqual(widget.Id, id);
        }
    }
}
