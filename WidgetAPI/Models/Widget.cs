﻿namespace WidgetAPI.Models
{
    public class Widget
    {
        public string Colour { get; set; }

        public int Id { get; set; }
    }
}
