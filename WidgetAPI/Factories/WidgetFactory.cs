﻿using System;
using WidgetAPI.Models;

namespace WidgetAPI.Factories
{
    public interface IFactory
    {
        Widget Make(int id, string colour);
    }

    public class WidgetFactory: IFactory
    {

        public Widget Make(int id, string colour)
        {
            if (id < 0)
            {
                var message = "Widgets must have an id greater than 0";
                throw new ApplicationException(message);
            }

            return new Widget()
            {
                Id = id,
                Colour = colour
            };
        }
    }
}
