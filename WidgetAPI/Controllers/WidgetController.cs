﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WidgetAPI.Models;
using WidgetAPI.Services;

namespace WidgetAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WidgetController : ControllerBase
    {
        private readonly IService _service;

        public WidgetController(IService service)
        {
            _service = service;
        }

        // GET api/widget
        [HttpGet]
        public ActionResult<IEnumerable<Widget>> Get()
        {
            var widgets = _service.PackWidgets(1, "blue", 6);
            return Ok(new { widgets });
        }
    }
}
