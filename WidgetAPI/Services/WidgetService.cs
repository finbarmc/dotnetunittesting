﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using WidgetAPI.Factories;
using WidgetAPI.Models;

namespace WidgetAPI.Services
{
    public interface IService
    {
        IEnumerable<Widget> PackWidgets(int id, string colour, int widgetPacketSize);
    }

    public class WidgetService : IService
    {
        private readonly ILogger<WidgetService> _logger;

        private readonly IFactory _factory;

        public WidgetService(ILogger<WidgetService> logger, IFactory factory)
        {
            if ((logger == null) || (factory == null))
            {
                throw new NullReferenceException("Parameters must be provided to the WidgetService");
            }

            _logger = logger;
            _factory = factory;
        }


        public IEnumerable<Widget> PackWidgets(int id, string colour, int widgetPacketSize)
        {
            if ((string.IsNullOrEmpty(colour) || (string.IsNullOrWhiteSpace(colour))))
            {
                throw new FormatException("Widgets need a colour");
            }

            List<Widget> widgets = new List<Widget>();
            try
            {
                for (int loopCounter = 1; loopCounter <= widgetPacketSize; loopCounter++)
                {
                    widgets.Add(_factory.Make(id, colour));
                }
            }
            catch(Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);
                throw e;
            }

            return widgets;
        }
    }
}