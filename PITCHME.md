# Unit Testing in .Net

---
## What Are We Doing This?
- Fast Feedback. Unit tests are significantly faster than functional or integration tests
- Protect against regression issues. A good test suite gives you the confidence to make changes.
- Less coupling in code. Unit testing enforces better software design - if generally is something is hard to unit test it's because of a code smell. 

---
## What Is A Good Unit Test?
# **F-I-R-S-T**
---
## What Is A Good Unit Test?
- Fast. It is not uncommon for mature projects to have thousands of unit tests. Unit tests should take very little time to run. Milliseconds.

---
## What Is A Good Unit Test?
- Isolated. Unit tests are standalone, can be run in isolation, and have no dependencies on any outside factors such as a file system or database.

---
## What Is A Good Unit Test?
- Repeatable. Running a unit test should be consistent with its results, that is, it always returns the same result if you do not change anything in between runs.

---
## What Is A Good Unit Test?
- Self-Checking. The test should be able to automatically detect if it passed or failed without any human interaction.

---
## What Is A Good Unit Test?
- Timely. A unit test should not take a disproportionately long time to write compared to the code being tested. If you find testing the code taking a large amount of time compared to writing the code, consider a design that is more testable.

---
## What Does a Unit Test Look Like?
A good unit test will have three phases:
- Arrange - setup what's required for the System Under Test (SUT)
- Act - exercise the system
- Assert - validate what happened
---

## Testing With Test Doubles
---
## Fakes
A fake is a generic term which can be used to describe either a stub or a mock object. Whether it is a stub or a mock depends on the context in which it's used. So in other words, a fake can be a stub or a mock.

---
## Mocks
A mock object is a fake object in the system that decides whether or not a unit test has passed or failed. A mock starts out as a Fake until it is asserted against.

---
## Stubs 
A stub is a controllable replacement for an existing dependency (or collaborator) in the system. By using a stub, you can test your code without dealing with the dependency directly. By default, a fake starts out as a stub.

---
## Overview of Dependency Injection
Dependency injection is a massive topic in it's own right, certainly more than we can cover in this session. The main types of injection we could use are:
- Constructor injection
- Parameter injection
- Field injection
---

## Unit Testing With MSTest 
MSTest is the framework most of our unit tests seems to use. The code example uses MSTest. It's probably worth reading the documentation. A good cheat sheet is [here](https://www.automatetheplanet.com/mstest-cheat-sheet/).
---
## Unit Testing With MSTest (cont)
Test attributes are used to markup methods within the test class. Some useful attribures are
- [TestMethod] - used to mark a method as a test method
- [TestClass] - used to mark a class as a test class
- [TestInitialize] - triggered before every test case
---
## Unit Testing With MSTest (cont)
- [TestCleanup] - triggered after every test case
- [OneTimeSetUp] - one-time triggered method before test cases start
- [OneTimeTearDown] - one-time triggered method after test cases end
- [Ignore] - Ensures a test is ignored by the runner
- [DataRow] - Configures a test to be data driven

---
## Unit Testing With NUnit
NUnit is a mature test framework that can be used by .Net Core and .Net Framework. Organisation Service uses NUnit. It's probably worth reading the documentation for that. A good primer is [here](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-nunit).

---
## Unit Testing With NUnit (cont)
Test attributes are used to markup methods within the test class. Some useful attribures are
- [Test] - used to mark a method as a test method
- [SetUp] - triggered before every test case
---
## Unit Testing With NUnit (cont)
- [TearDown] - triggered after every test case
- [ClassInitialize] - one-time triggered method before test cases start
- [ClassCleanup] - one-time triggered method after test cases end
- [Ignore] - Ensures a test is ignored by the runner
- [TestCase] - Configures a test to be data driven

---

## Best Practices
- Name your unit tests correctly
- Arrange, Act & Assert: This is the standard format for a unit test. Setup what you need to execute the test, carry out the action your validating in the System Under Test (SUT)
- Use Moq to setup mocks of dependencies your unit needs. Use it to verify your expectations
- Avoid magic strings where possible

---
## Best Practices (cont)
- Avoid creating setup objects yourself where possible
- Avoid logic in tests - ifs, loops etc
- Avoid too many asserts (some have the opinion of 1 per test, which may not be the most pragmatic)
- Only test the public interface (treat private methods as implementation detail)

---
## Best Practices (cont)
- Consider wrapping hard dependencies in an interface. When you need to mock classes that don't implement an interface, it can be useful to wrap them in a class that does, so you can mock them effectively 

---
## Moq
Moq is a mocking library for .Net. It supports mocking interfaces as well as classes and makes good use of LINQ expression trees/lambda expressions.

It's worth reading their guide [here](https://github.com/Moq/moq4/wiki/Quickstart).
---
## Moq
The Moq documentation has a full guide to the API it provides for creating mocks. Some key methods are:
- Setup
- Verify

---
## Moq Setup
Setup a mock so that it returns a certain value, throw an exception etc. 
```csharp
_factoryMock.Setup(a => a.Make(It.IsAny<int>(), It.IsAny<string>()))
.Throws(new FormatException(exceptionMessage));
```

---
## Moq Verify
Evaluates the mock and verifies a given expectation was met (e.g. a specific method was called once)
```sh
_factoryMock.Verify(f => f.Make(It.IsAny<int>(), It.IsAny<string>()),
Times.Exactly(expectedWidgets.Count()));
```

---
## AutoFixture
When writing unit tests, you typically need to create some objects that represent the initial state of the test.

---
AutoFixture is a great library for generating instances of a class for you, which is particularly useful where you have to create a complex object as part of a test setup.

Not having to code up objects by hand in our test setup makes tests easier to maintain and quicker to write.
---

## Running Tests From The Command Line
For .Net Core you can run tests from the command line using the command below (inside the same directory as the test .csproj)

```csharp
dotnet test
```
Detail on running .Net Framework tests from the command line is [here](https://docs.microsoft.com/en-us/visualstudio/test/vstest-console-options?view=vs-2017).
---


## Links
- [Microsoft Guidelines for Unit Testing](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-best-practices) 
- [.Net Testing with MSTest](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-mstest)
- [MSTest Cheat Sheet](https://www.automatetheplanet.com/mstest-cheat-sheet/)
- [.Net Testing with xUnit](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test)
- [.Net Testing with nUnit](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-nunit)
- [Moq Quickstart Documentation](https://github.com/Moq/moq4/wiki/Quickstart)
- [AutoFixture Cheat Sheet](https://github.com/AutoFixture/AutoFixture/wiki/Cheat-Sheet)
- [Test Doubles, Fakes, Stubs & Mocks](https://adamcod.es/2014/05/15/test-doubles-mock-vs-stub.html)


---
